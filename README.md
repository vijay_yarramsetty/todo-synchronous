### FastAPI simple CRUD api end points in synchronous python

#### Project Setup
* Create a virtual environment for this FastAPI application
* Install the dependencies from requirements.txt
* run the application on uvicorn webserver using command `uvicorn main:app --reload` or run the **main.py** file from CLI ex: `python3 main.py`
* 