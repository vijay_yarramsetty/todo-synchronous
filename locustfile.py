from locust import HttpUser, task, between


class WebsiteUser(HttpUser):
    wait_time = between(1, 5)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.id = None

    def on_start(self):
        pass

    def on_stop(self):
        pass

    @task
    def get_root(self):
        self.client.get("/", name="Root")

    @task
    def get_posts(self):
        self.client.get('/posts', name="GetPosts")

    @task(2)
    def add_post(self):
        res = self.client.post("/posts", json={"title": "post 123 test", "content": "post 123 test content"}, name="AddPost")
        self.id = res.json()['id']

    @task
    def get_post(self):
        self.client.get("/posts/1", name="GetPost")

    @task
    def update_post(self):
        self.client.put("/posts/1", json={"title": "Post title updated", "content": "Post content updated"}, name="UpdatePost")

    @task
    def delete_post(self):
        if self.id:
            self.client.delete(f"/posts/{self.id}", name="DeletePost")
