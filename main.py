from fastapi import FastAPI
import uvicorn
from src import models
from src.database import engine
from src.routes import router

# create tables
models.Base.metadata.create_all(bind=engine)

# FastAPI application instace
app = FastAPI()

# adding the Post routes located in package src, routes module
app.include_router(router)


if __name__ == "__main__":
    uvicorn.run("main:app", port=8000, reload=True)
