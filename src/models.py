from sqlalchemy import Column, Integer, String
from .database import Base


class Post(Base):
    """
    Creating a Post Class which gets converted to 'posts' table in database
    """
    __tablename__ = "posts"
    id = Column(Integer, primary_key=True)
    title = Column(String(128))
    content = Column(String(256))

    def __repr__(self):
        return f"<Post(Title: {self.title})>"
