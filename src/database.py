from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

# Postgres db connection with user=vijay, db=vijay
SQLALCHEMY_DATABASE_URL = "postgresql+psycopg2://vijay:test@localhost/vijay"

# Sqlite db URI
# SQLALCHEMY_DATABASE_URL = "sqlite:///db.sqlite"

# Creating engine
engine = create_engine(SQLALCHEMY_DATABASE_URL, echo=False)

# Creating a single session that can be used anytime
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
Base = declarative_base()


def get_db():
    # accessing db session instace
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()
