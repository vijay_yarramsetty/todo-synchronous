from fastapi import Depends, HTTPException, APIRouter
from sqlalchemy.orm import Session
from typing import List
from fastapi.responses import JSONResponse


# relative imports from same package
from . import models
from . import schemas
from .database import get_db

# creating a router instance for routes module
router = APIRouter()


@router.get('/')
def read_root():
    return {"message": "Hello World"}


@router.get('/posts', response_model=List[schemas.Post])
def get_posts(skip: int = 0, db: Session = Depends(get_db)):
    posts = db.query(models.Post).offset(skip).all()
    return posts


@router.get('/posts/{post_id}', response_model=schemas.Post)
def get_post(post_id: int, db: Session = Depends(get_db)):
    post = db.query(models.Post).filter(models.Post.id == post_id).first()
    if post is None:
        raise HTTPException(status_code=404, detail="Post not found")
    return post


@router.post('/posts', response_model=schemas.Post)
def add_post(post: schemas.PostCreate, db: Session = Depends(get_db)):
    post_item = models.Post(**post.dict())
    db.add(post_item)
    db.commit()
    db.refresh(post_item)
    return post_item


@router.delete('/posts/{post_id}')
def delete_post(post_id: int, db: Session = Depends(get_db)):
    post = db.query(models.Post).filter(models.Post.id == post_id).first()
    if post is None:
        raise HTTPException(status_code=404, detail="Post not found")
    db.delete(post)
    db.commit()
    return JSONResponse(status_code=202, content={"message": f"post id {post.id} deleted successfully"})


@router.put('/posts/{post_id}', response_model=schemas.Post)
def update_post(post_id: int, post: schemas.PostCreate, db: Session = Depends(get_db)):
    post_item = db.query(models.Post).filter(models.Post.id == post_id).first()
    if post is None:
        raise HTTPException(status_code=404, detail="Post not found for update")
    post_item.title = post.dict()['title']
    post_item.content = post.dict()['content']
    db.commit()
    db.refresh(post_item)
    return post_item
