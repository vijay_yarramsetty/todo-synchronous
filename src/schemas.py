from pydantic import BaseModel


# Pydantic models of Post model for data conversion
class PostBase(BaseModel):
    title: str
    content: str


class PostCreate(PostBase):
    pass


# response model to be sent back in api
class Post(PostBase):
    id: int

    class Config:
        orm_mode = True
